const getDistance = (point, startLinePoint, finishLinePoint) => {
  const { longitude: pointX, latitude: pointY } = point;
  const { longitude: startLineX, latitude: startLineY } = startLinePoint;
  const { longitude: finishLineX, latitude: finishLineY } = finishLinePoint;

  const doubleArea = Math.abs((finishLineY - startLineY) * pointX -
  (finishLineX - startLineX) * pointY + finishLineX * startLineY - finishLineY * startLineX);

  lineSegmentLength = Math.sqrt(Math.pow((finishLineX - startLineX), 2) +
  Math.pow((finishLineY - startLineY), 2));

  if (lineSegmentLength !== 0.0) {
    return doubleArea / lineSegmentLength;
  }

  return 0.0;
}

const simplify = (firstIndex, lastIndex, points, tolerant, markedPoints) => {
  if (lastIndex === firstIndex + 1) return;

  let maxDistance = -1;
  let maxIndex = lastIndex;

  for (let i = firstIndex + 1; i < lastIndex; i++) {
    distance = getDistance(points[i], points[firstIndex], points[lastIndex]);

    if (distance > maxDistance) {
      maxDistance = distance;
      maxIndex = i;
    }
  }

  if (maxDistance > tolerant) {
    console.log(`get maxDistance: ${maxDistance}, index: ${maxIndex}, value: ${points[maxIndex]}`);
    simplify(firstIndex, maxIndex, points, tolerant, markedPoints);
    markedPoints.push(points[maxIndex]);
    simplify(maxIndex, lastIndex, points, tolerant, markedPoints);
  }
};

/**  */
const getSimplificatedPolyline = (points, tolerant) => {
  if (points.length < 3) {
    return points;
  }

  const markedPoints = [];

  markedPoints.push(points[0]);
  simplify(0, points.length - 1, points, tolerant, markedPoints);
  markedPoints.push(points[points.length - 1]);
  
  return markedPoints;
};

module.exports = {
  getSimplificatedPolyline,
}
