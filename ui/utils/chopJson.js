const fs = require("fs");

const chopJson = (points) => {
  let parsedPoints = [];
  let parsedSegment = [];

  let isFake = points[0].is_fake;
  let isDisabled = points[0].is_disabled;
  let isNoData = points[0].is_no_data;
  let isPowerSaving = points[0].is_power_saving;
  let isEmptyRun = points[0].is_empty_run;

  points.forEach((point, index) => {
    if (
      (point.events[0] && index !== 0) || // наличие одного или более событий завершает сегмент
      isFake !== point.is_fake ||
      isDisabled !== point.is_disabled ||
      isNoData !== point.is_no_data ||
      isPowerSaving !== point.is_power_saving ||
      isEmptyRun !== point.is_empty_run
    ) {
      isFake = point.is_fake;
      isDisabled = point.is_disabled;
      isNoData = point.is_no_data;
      isPowerSaving = point.is_power_saving;
      isEmptyRun = point.is_empty_run;

      parsedPoints.push(parsedSegment);
      parsedSegment = [];
    }

    parsedSegment.push(point);
  });

  parsedPoints.push(parsedSegment);
  parsedSegment = [];

  console.log("parsedPoints", parsedPoints);
  console.log("segment count", parsedPoints.length);

  return parsedPoints;
};

module.exports = {
  chopJson,
}

