const fs = require("fs");
const { getSimplificatedPolyline } = require("./utils/getSimplificatedPolyline");
const { chopJson } = require("./utils/chopJson");

// chopJson();

const makePoints = (segments) => {
  console.log(segments);

  const newTrace = [];

  segments.forEach(segment => {
    const newPoints = getSimplificatedPolyline(segment, 0.0005);

    console.log("newPoints", newPoints);

    newPoints.forEach(newPoint => newTrace.push(newPoint));
  });

  console.log(newTrace);
  console.log("all done!!");

  return newTrace;
};

fs.readFile("../inf/5k_source_mod.json", "utf8", (err, data) => {
  if (err) {
    console.log("err", err);
    return;
  }

  const obj = JSON.parse(data);
  const points = obj.traces[0].points;

  const chopTrace = chopJson(points);

  obj.traces[0].points = makePoints(chopTrace);

  const json = JSON.stringify(obj);

  fs.writeFile('../out/5k_source_mod_opti.json', json, (err) => {
    if (err) {
      console.log("err", err);
      return;
    }

    console.log("file saved");
  });
});
